#!/bin/bash

NAME="jenkins"
ZONE="europe-west1-c"
PROJ="ubuntu-os-cloud"
FMLY="ubuntu-1604-lts"
TAGS="http-server,https-server"

gcloud -q compute instances create ${NAME} --zone ${ZONE} --image-family ${FMLY} --image-project ${PROJ} --address ${NAME} --tags ${TAGS}
IP=$(gcloud compute instances list ${NAME} | grep ${NAME} | awk '{print $5}')
sleep 20
gcloud -q compute ssh ${NAME} -- hostname
ansible-playbook -i inventory jenkins.yml

# gcloud -q compute instances delete ${NAME}
sed -i'.bak' "/${IP}/d" ~/.ssh/known_hosts
